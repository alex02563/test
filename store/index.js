export const state = () => ({
    modalshow:false,
    moddata:[],
    ticketitmodalshow:false,
    ticketidata:[],
    loading:false,
    memberdata:[]
    })
export const getters = {
    getmodalshow: (state) => { return state.modalshow },
    getmoddata: (state) => { return state.moddata },
    getmember: (state) => { return state.memberdata },
    getticketitmodalshow: (state) => { return state.ticketitmodal },
    ticketitmodaldata: (state) => { return state.ticketidata },
    getmodloading: (state) => { return state.loading },
    
}
export const mutations = {
    togglemodalshow (state, bool ) {
        state.modalshow = !state.modalshow
    },
    togglemodaldata (state, data) {
        state.moddata = data
    },
    toggleticketitshow (state, bool ) {
        state.ticketitmodalshow = !state.ticketitmodalshow
    },
    togglticketitdata (state, data) {
        state.ticketidata = data
    },
    togglememberdata (state, data) {
        state.memberdata = data
    },
    toggleloading (state, bool) {
        state.loading = !state.loading
    },
    SET_LANG(state, locale) {
        if (state.locales.indexOf(locale) !== -1) {
            state.locale = locale
        }
    }
}